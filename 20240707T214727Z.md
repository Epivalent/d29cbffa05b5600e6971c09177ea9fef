Certainly! If you're interested in historical documentaries that are engaging but perhaps a bit less intense than "Pompeii - The Last Day," here are a few suggestions:

1. **"Rome: Engineering an Empire"**
   This series explores the incredible engineering feats of ancient Rome, from aqueducts to roads and monumental buildings. It provides fascinating insights into how these structures were built and their impact on Roman society, without the heavy doom-laden atmosphere.

2. **"Secrets of the Dead"**
   This long-running PBS series delves into various historical mysteries and events with a focus on uncovering new evidence and perspectives. Each episode covers different topics, ranging from ancient civilizations to more recent history, offering a balanced mix of intrigue and education.

3. **"Ancient Discoveries"**
   This show investigates technological innovations from ancient times, revealing how many modern inventions have roots in antiquity. It's an enlightening look at human ingenuity throughout history, presented in an accessible and engaging manner.

These programs should provide you with captivating historical content while maintaining a lighter tone compared to the dramatic retelling of Pompeii's last days.

# Sun  7 Jul 21:47:27 CEST 2024 - suggest two or three programmes a bit like Pompeii - The Last Day but perhaps less portentious and doomy